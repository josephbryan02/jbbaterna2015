from bson.objectid import ObjectId

class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, username, mood):
        self.conn.insert({'post':post, 'username': username, 'mood' : mood})

    def updatePost(self, newPost, postID, mood):
    	self.conn.update({'_id' : ObjectId(postID)}, {"$set": {'post' : newPost, 'mood' : mood}})